document.getElementsByName("txtValor1")[0].placeholder = `
EXAMPLE
-------

  /* 1 */
  { 
    "BBB" : "1",
    "CCC" : "",
    "DDD" : "AAA",
    "FFF" : NumberLong(0),
    "GGG" : ISODate("2022-06-27T09:10:42.024Z"),
    "HHH" : NumberDecimal("0.00"),
  }
  /* 2 */
  { 
    "BBB" : "1",
    "CCC" : "",
    "DDD" : "AAA",
    "FFF" : NumberLong(0),
    "GGG" : ISODate("2022-06-27T09:10:42.024Z"),
    "HHH" : NumberDecimal("0.00"),
  }
  /* 3 */
  { 
    "BBB" : "1",
    "CCC" : "",
    "DDD" : "AAA",
    "FFF" : NumberLong(0),
    "GGG" : ISODate("2022-06-27T09:10:42.024Z"),
    "HHH" : NumberDecimal("0.00"),
  }
  `

const message = document.getElementById("txtValor1")
const messageOut = document.getElementById("txtValor2")
const btnReemplazar = document.getElementById("btnReemplazar")

const fnTransfer = () => {
  try {
    let data = `[${message.value}]`

    const out1 = data
      .replace(/\s*?\/\*\s\d+\s\*\//gm, ",")
      .replace(/\,/, "")
      .replace(/[a-zA-Z]+\(/gm, "")
      .replace(/\)\,/gm, ",")

    let title = ""
    let values = ""

    JSON.parse(out1).map((obj, i) => {
      Object.keys(obj).map((key, j, { length }) => {
        if (i == 0) {
          title += `${key ?? ""};`
        }

        if (key === "IMP_ASIMSC") {
          // if (key === "IMP_ASIMSC" || key === "IMP_FIXING") {
          obj[key] = obj[key].replace(".", ",")
        }

        values += `${obj[key] ?? ""};`

        if (j + 1 === length) {
          values += "\r\n"
        }
      })
    })

    //title.join(";")

    messageOut.innerHTML = `${title}\r\n${values}`
  } catch (err) {
    console.log(err)
  }
}

btnReemplazar.onclick = () => {
  fnTransfer()
}

message.onchange = () => {
  fnTransfer()
}

var copyTextareaBtn = document.querySelector("#btnCopy")

copyTextareaBtn.addEventListener("click", function (event) {
  var copyText = messageOut

  copyText.select()
  copyText.setSelectionRange(0, 99999)

  navigator.clipboard.writeText(copyText.value)
})
